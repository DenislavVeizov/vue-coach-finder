export default {
  blockUI(context) {
    context.commit("blockUI");
  },
  unblockUI(context) {
    context.commit("unblockUI");
  }
};
