import { createStore } from "vuex";
import modules from "@/store/modules";
import mutations from "@/store/mutations";
import actions from "@/store/actions";
import getters from "@/store/getters";

export default createStore({
  modules,
  state() {
    return {
      userId: "c4",
      loading: null
    };
  },
  getters,
  actions,
  mutations
});
