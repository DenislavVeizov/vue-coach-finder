export default {
  async registerCoach(context, data) {
    const userId = context.rootGetters.userId;
    const coachData = {
      firstName: data.firstName,
      lastName: data.lastName,
      areas: data.areas,
      description: data.description,
      hourlyRate: data.hourlyRate
    };

    const response = await fetch(
      `https://vue-coach-finder-6edfc-default-rtdb.firebaseio.com/coaches/${userId}.json`,
      {
        method: "PUT",
        body: JSON.stringify(coachData)
      }
    );
    if (!response.ok) {
      //error...
    }
    // const responseData = await response.json();
    context.commit("registerCoach", {
      ...coachData,
      id: userId
    });
  },
  async loadCoaches(context, data) {
    if (!data.forceRefresh && !context.getters.shouldUpdate) {
      return;
    }

    const response = await fetch(
      `https://vue-coach-finder-6edfc-default-rtdb.firebaseio.com/coaches.json`
    );
    let responseData = await response.json();
    if (!response.ok) {
      throw new Error(responseData.message || "Failed to fetch!");
    }
    const coaches = [];
    for (const key in responseData) {
      const coach = {
        id: key,
        firstName: responseData[key].firstName,
        lastName: responseData[key].lastName,
        areas: responseData[key].areas,
        description: responseData[key].description,
        hourlyRate: responseData[key].hourlyRate
      };
      coaches.push(coach);
    }
    context.commit("setCoaches", coaches);
    context.commit("setFetchTimestamp");
  }
};
