export default {
  async contactCoach(context, data) {
    const newRequest = {
      userEmail: data.email,
      message: data.message
    };

    const response = await fetch(
      `https://vue-coach-finder-6edfc-default-rtdb.firebaseio.com/requests/${data.coachId}.json`,
      {
        method: "POST",
        body: JSON.stringify(newRequest)
      }
    );
    if (!response.ok) {
      throw new Error(responseData.message || "Failed to send request!");
    }

    let responseData = await response.json();
    newRequest.id = responseData.name;
    newRequest.coachId = responseData.name;
    context.commit("addRequest", newRequest);
  },
  async fetchRequests(context) {
    const coachId = context.rootGetters.userId;
    const response = await fetch(
      `https://vue-coach-finder-6edfc-default-rtdb.firebaseio.com/requests/${coachId}.json`
    );
    let responseData = await response.json();
    if (!response.ok) {
      throw new Error(responseData.message || "Failed to fetch requests!");
    }

    const requests = [];
    for (const key in responseData) {
      const request = {
        id: key,
        coachId: coachId,
        message: responseData[key].message,
        userEmail: responseData[key].userEmail
      };
      requests.push(request);
    }
    context.commit("setRequests", requests);
  }
};
