export default {
  requests(state, _, _2, rootGetters) {
    const loggedUserId = rootGetters.userId;
    return state.requests.filter(req => req.coachId === loggedUserId);
  },
  hasRequests(_, getters) {
    return getters.requests && getters.requests.length > 0;
  }
};
