import { ElLoading } from "element-plus";
export default {
  blockUI(state) {
    state.loading = ElLoading.service({
      lock: true,
      text: "Loading",
      spinner: "el-icon-loading",
      background: "rgba(0, 0, 0, 0.7)"
    });
  },
  unblockUI(state) {
    if (state.loading) {
      state.loading.close();
    }
  }
};
