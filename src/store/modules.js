import coachModule from "@/store/modules/coach";
import requestModule from "@/store/modules/request";

export default {
  coachModule,
  requestModule
}