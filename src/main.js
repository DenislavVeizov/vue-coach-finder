import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementPlus from "element-plus";
import "element-plus/lib/theme-chalk/index.css";
import locale from "element-plus/lib/locale/lang/bg";
import BaseBadge from "@/components/base/BaseBadge";

let app = createApp(App);
app.use(store);
app.use(router);
app.use(ElementPlus, { locale });
app.component("base-bade", BaseBadge);
app.mount("#app");
