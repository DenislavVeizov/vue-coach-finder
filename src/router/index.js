import { createRouter, createWebHashHistory } from "vue-router";
import CoachesList from "@/pages/coach/CoachesList";
import CoachDetail from "@/pages/coach/detail/CoachDetail";
import ContactCoach from "@/pages/coach/detail/contact/CoachContact";
import CoachRegistration from "@/pages/coach/registration/CoachRegistration";
import RequestsList from "@/pages/request/RequestsList";
import PageNotFound from "@/pages/basic/PageNotFound";

const routes = [
  { path: "/", redirect: "/coaches" },
  { name: "coaches", path: "/coaches", component: CoachesList },
  {
    props: true,
    name: "coachDetails",
    path: "/coaches/:id",
    component: CoachDetail,
    children: [{ name: "contactCoach" , path: "contact", component: ContactCoach }]
  },
  { name: "registration", path: "/registration", component: CoachRegistration },
  { name: "requests", path: "/requests", component: RequestsList },
  { path: "/:notFound(.*)", component: PageNotFound }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
